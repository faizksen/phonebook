package com.faiz.phonebook.util;

public class PermissionRequestResult {

    public boolean isGranted;

    public PermissionRequestResult(boolean isGranted) {
        this.isGranted = isGranted;
    }


}
