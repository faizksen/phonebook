package com.faiz.phonebook.util;

import android.util.SparseArray;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.NoSuchElementException;

public abstract class MultiTypeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private SparseArray<BaseAdapterDispatcher> dispatchers = new SparseArray<>();

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == -1) {
            throw new IllegalArgumentException("type can't be -1 in " + this.getClass().getSimpleName());
        }
        BaseAdapterDispatcher baseAdapterDispatcher = dispatchers.get(viewType);
        if (baseAdapterDispatcher == null) {
            throw new NoSuchElementException("no dispatcher registered for type " + viewType);
        }
        return baseAdapterDispatcher.onCreateViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int itemType = getItemViewType(position);
        BaseAdapterDispatcher dispatcher = dispatchers.get(itemType);
        if (dispatcher != null) {
            dispatcher.onBindViewHolder(holder, getItem(position), position, null);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position, @NonNull List<Object> payloads) {
        int itemType = getItemViewType(position);
        BaseAdapterDispatcher dispatcher = dispatchers.get(itemType);
        if (dispatcher != null) {
            dispatcher.onBindViewHolder(holder, getItem(position), position, payloads);
        }
    }

    public abstract Object getItem(int position);

    public void registerType(int type, BaseAdapterDispatcher dispatcher) {
        dispatchers.put(type, dispatcher);
    }

    public BaseAdapterDispatcher getDispatcherForType(int type){
        return dispatchers.get(type);
    }
}
