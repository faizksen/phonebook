package com.faiz.phonebook.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.faiz.phonebook.R;
import com.faiz.phonebook.widget.CustomProgressDrawable;
import com.faiz.phonebook.widget.EndlessRecyclerView;

public class ListLoadingDecoration extends RecyclerView.ItemDecoration {

    private final EndlessRecyclerView recyclerView;
    private InvalidateRunnable invalidateRunnable = new InvalidateRunnable();
    private Rect rect = new Rect(0, 0, 0, 0);
    private ImageView imageView;
    private CustomProgressDrawable progressDrawable;
    private int height, navigationHeight;
    private Paint bgPaint;
    private int marginLeft;

    public ListLoadingDecoration(Context context, EndlessRecyclerView recyclerView) {
        this(context, recyclerView, context.getResources().getColor(R.color.divider_color));
    }

    public ListLoadingDecoration(Context context, EndlessRecyclerView recyclerView, int color) {
        this(context, recyclerView, color, 0);
    }

    public ListLoadingDecoration(Context context, EndlessRecyclerView recyclerView, int color, int marginLeft) {
        this.recyclerView = recyclerView;
        imageView = new ImageView(context);
        this.marginLeft = marginLeft;

        Drawable drawable = context.getResources().getDrawable(R.drawable.ic_progress);
        progressDrawable = new CustomProgressDrawable(drawableToBitmap(drawable));
        imageView.setImageDrawable(progressDrawable);
        progressDrawable.start();

        bgPaint = new Paint();
        bgPaint.setColor(color);

        int loaderSize = context.getResources().getDimensionPixelSize(R.dimen.list_loading_progress_size);
        imageView.measure(View.MeasureSpec.makeMeasureSpec(loaderSize, View.MeasureSpec.EXACTLY), View.MeasureSpec.makeMeasureSpec(loaderSize, View.MeasureSpec.EXACTLY));

        int left;
        if (marginLeft != 0) {
            left = marginLeft;
        } else {
            int widthPixels = context.getResources().getDisplayMetrics().widthPixels;
            left = widthPixels / 2 - imageView.getMeasuredWidth()/2;
        }

        imageView.layout(left, 0, left + imageView.getMeasuredWidth(), imageView.getMeasuredHeight());

        height = context.getResources().getDimensionPixelSize(R.dimen.list_loading_view_height);
        navigationHeight = context.getResources().getDimensionPixelSize(R.dimen.toolbar_height);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        if (!recyclerView.isLoading() || !recyclerView.isEndless()) {
            return;
        }

        int position = parent.getChildAdapterPosition(view);
        if (position == parent.getAdapter().getItemCount() - 1) {
            int marginBottom = navigationHeight;
            outRect.bottom = height + marginBottom;
        }

    }

    @Override
    public void onDraw(final Canvas c, final RecyclerView parent, final RecyclerView.State state) {
        super.onDraw(c, parent, state);

        if (!recyclerView.isLoading()) {
            return;
        }

        final View child = parent.getChildAt(parent.getChildCount() -1);
        if (child == null) {
            return;
        }


        Rect outRect = new Rect();
        parent.getDecoratedBoundsWithMargins(child, outRect);
        int topOffset = outRect.height() - (child.getHeight() + height);
        topOffset = Math.max(topOffset, 0);

        int dividerLeft = parent.getPaddingLeft();
        int dividerRight = parent.getWidth() - parent.getPaddingRight();

        int dividerTop = child.getBottom() + topOffset;
        int dividerBottom = dividerTop + height;

        rect.set(dividerLeft, dividerTop, dividerRight, dividerBottom);

        c.drawRect(dividerLeft, dividerTop, dividerRight, dividerBottom, bgPaint);

        c.save();

        int x;
        if (marginLeft == 0) {
            x = dividerLeft + (parent.getWidth() - imageView.getMeasuredWidth()) / 2;
        } else {
            x = dividerLeft + marginLeft;
        }

        int marginBottom = navigationHeight;

        int y = dividerTop + (height - imageView.getMeasuredHeight())/2 - marginBottom;
        c.translate(x, y);

        imageView.draw(c);

        c.restore();


        parent.removeCallbacks(null);
        parent.post(invalidateRunnable);
    }

    private static Bitmap drawableToBitmap (Drawable drawable) {

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable)drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    private class InvalidateRunnable implements Runnable {

        @Override
        public void run() {
            recyclerView.invalidate(rect);
        }
    }

}
