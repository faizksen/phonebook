package com.faiz.phonebook.util;

import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public abstract class BaseAdapterDispatcher<S extends RecyclerView.ViewHolder, T> {

    public abstract S onCreateViewHolder(ViewGroup parent);

    public abstract void onBindViewHolder(S holder, T object, int position, List<Object> payloads);
}
