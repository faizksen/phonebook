package com.faiz.phonebook.data.model;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.io.Serializable;

public class Phone implements Serializable {

    private String contactId;
    private String phoneNumber;

    public Phone() {
    }

    public Phone(String id, String content) {
        contactId = id;
        this.phoneNumber = content;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getPhoneNumber() {
        return phoneNumber == null ? "" : phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null)
            return false;
        if (obj == this)
            return true;
        if (obj.getClass() != getClass())
            return false;
        return TextUtils.equals(phoneNumber, ((Phone) obj).getPhoneNumber());
    }
}
