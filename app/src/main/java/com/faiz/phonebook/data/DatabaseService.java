package com.faiz.phonebook.data;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.text.TextUtils;

import com.faiz.phonebook.data.model.BaseObject;
import com.faiz.phonebook.data.model.Contact;
import com.faiz.phonebook.data.realm.SavedStateBundle;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class DatabaseService {

    private static ExecutorService executorService = Executors.newSingleThreadExecutor();

    private Realm realm;
    private Realm savedRealm;

    public DatabaseService (Context context) {
        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("phonebook.realm")
                .build();
        try {
            realm = Realm.getInstance(config);
        } catch (Exception e) {
            e.printStackTrace();
            Realm.deleteRealm(config);
            realm = Realm.getInstance(config);
        }
        initSavedStateRealm();
    }

    private void initSavedStateRealm(){
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("savedState.realm")
                .build();

        try {
            savedRealm = Realm.getInstance(config);
        } catch (Exception e) {
            e.printStackTrace();
            Realm.deleteRealm(config);
            savedRealm = Realm.getInstance(config);
        }
    }

    public void closeRealm() {
        if (realm != null || !realm.isClosed()) {
            realm.close();
        }
        if (savedRealm != null || !savedRealm.isClosed()) {
            savedRealm.close();
        }
    }

    public void saveState(Bundle outData) {
        try {
            Parcel parcel = Parcel.obtain();
            outData.setClassLoader(ClassLoader.getSystemClassLoader());
            outData.writeToParcel(parcel, 0);
            byte[] bytes = parcel.marshall();
            parcel.recycle();

            SavedStateBundle savedStateBundle = new SavedStateBundle();
            savedStateBundle.setData(bytes);
            savedRealm.beginTransaction();
            savedRealm.insertOrUpdate(savedStateBundle);
            savedRealm.commitTransaction();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public Bundle getState() {
        try {
            SavedStateBundle savedStateBundle = savedRealm.where(SavedStateBundle.class).findFirst();
            if (savedStateBundle == null) {
                return null;
            }

            Parcel parcel = Parcel.obtain();
            byte[] data = savedStateBundle.getData();
            parcel.unmarshall(data, 0, data.length);
            parcel.setDataPosition(0); // This is extremely important!
            Bundle bundle = parcel.readBundle(BaseObject.class.getClassLoader());
            parcel.recycle();

            savedRealm.beginTransaction();
            savedRealm.deleteAll();
            savedRealm.commitTransaction();


            return bundle;
        } catch (Exception e) {
            return null;
        }
    }

    private void handleRealmException(Throwable throwable){
        if (realm.isInTransaction()) {
            realm.cancelTransaction();
        }
        throwable.printStackTrace();
    }
}
