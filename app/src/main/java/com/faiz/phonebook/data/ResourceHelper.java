package com.faiz.phonebook.data;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import androidx.annotation.PluralsRes;
import androidx.annotation.StringRes;
import android.util.DisplayMetrics;
import android.util.Log;

import java.lang.ref.WeakReference;

public class ResourceHelper {
    private WeakReference<Context> contextRef;

    public ResourceHelper(Context context) {
        this.contextRef = new WeakReference<Context>(context);
        Log.i("Info", "inited");
    }

    public Resources getResources(){
        if (contextRef == null || contextRef.get() == null){
            return null;
        }
        return contextRef.get().getResources();
    }

    public static int convertDpToPixel(int dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) px;
    }

    public static int converPixelToDp(int pixels, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = pixels / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) dp;
    }

    public String getString(@StringRes int stringId) {
        return getResources().getString(stringId);
    }

    public String getString(@StringRes int stringId, Object... formatArgs) {
        return getResources().getString(stringId, formatArgs);
    }

    public String getQuantityString(@PluralsRes int id, int quantity, Object... formatArgs) {
        return getResources().getQuantityString(id, quantity, formatArgs);
    }

    public static int parseColor(String colorHex, int defaultColor){
        try {
            return Color.parseColor(colorHex);
        }catch (IllegalArgumentException e){
            Log.wtf("Exception", new IllegalArgumentException(e.getMessage() + " " + colorHex));
            return defaultColor;
        }
    }
}
