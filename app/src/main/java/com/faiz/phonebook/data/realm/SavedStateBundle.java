package com.faiz.phonebook.data.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SavedStateBundle extends RealmObject {

    @PrimaryKey
    private String id = "saved_state_bundle";

    public byte[] data;

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
