package com.faiz.phonebook.data;

import android.content.Context;

import com.faiz.phonebook.PhoneBookApplication;

public class ServiceProvider {

    private static ServiceProvider instance;

    private Context applicationContext;
    private ResourceHelper resourceHelper;
    private StorageService storageService;
    private DatabaseService databaseService;
    private AppModel appModel;

    public static void makeInstance(Context context) {
        instance = new ServiceProvider(context);
    }

    public static ServiceProvider getInstance(){
        if (instance == null) {
            throw new NullPointerException("Service provider is not initialized");
        }
        return instance;
    }

    public ServiceProvider(Context context) {
        applicationContext = context;
        resourceHelper = new ResourceHelper(context);
        storageService = new StorageService(context);
        databaseService = new DatabaseService(context);
        appModel = new AppModel(context);
    }

    public ResourceHelper getResourceHelper() {
        return resourceHelper;
    }

    public StorageService getStorageService() {
        return storageService;
    }

    public DatabaseService getDatabaseService() {
        return databaseService;
    }

    public AppModel getAppModel() {
        return appModel;
    }

    public Context getApplicationContext(){
        if (applicationContext == null) {
            applicationContext = PhoneBookApplication.getInstance().getApplicationContext();
        }
        return applicationContext;
    }
}
