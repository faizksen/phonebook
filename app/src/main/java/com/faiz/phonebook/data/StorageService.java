package com.faiz.phonebook.data;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcel;

public class StorageService {

    public static final String PREFERENCES_NAME = "phone_book_pref";

    private SharedPreferences sharedPreferences;

    public StorageService(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, Application.MODE_PRIVATE);
    }
}
