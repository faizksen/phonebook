package com.faiz.phonebook.data;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.CallLog;
import android.provider.ContactsContract;

import androidx.core.app.ActivityCompat;

import com.faiz.phonebook.data.model.Call;
import com.faiz.phonebook.data.model.Contact;
import com.faiz.phonebook.ui.base.PermissionActivity;
import com.faiz.phonebook.util.PermissionRequestError;
import com.faiz.phonebook.util.PermissionRequestResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AppModel {

    private Context context;

    private ObservableEmitter<? super List<Contact>> readContactsEmitter;
    private ObservableEmitter<? super List<Call>> readCallLogEmitter;

    public AppModel (Context context) {
        this.context = context;
    }

    public <T> ObservableTransformer<T, T> applyAsyncSchedulers() {
        return new ObservableTransformer<T, T>() {
            @Override
            public ObservableSource<T> apply(Observable<T> upstream) {
                return upstream
                        .subscribeOn(Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR))
                        .observeOn(AndroidSchedulers.mainThread());
            }
        };
    }

    public Observable<List<Contact>> requestReadContacts(final PermissionActivity currentActivity) {
        Observable<List<Contact>> readContactsObservable = Observable
                .create(emitter -> {
                    readContactsEmitter = emitter;
                    int permissionState = ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS);
                    if (permissionState != PackageManager.PERMISSION_GRANTED) {
                        requestReadContactPermission(currentActivity);
                    } else {
                        getContacts();
                    }
                });
        return readContactsObservable.compose(applyAsyncSchedulers());
    }

    private void requestReadContactPermission(PermissionActivity currentActivity) {
        currentActivity
                .requestPermission(Manifest.permission.READ_CONTACTS)
                .subscribe(
                        permissionRequestResult -> {
                            if (permissionRequestResult.isGranted) {
                                getContacts();
                            } else {
                                readContactsEmitter.onError(new PermissionRequestError());
                                readContactsEmitter = null;
                            }
                        },
                        error -> {
                            PermissionRequestError permissionRequestError = new PermissionRequestError();
                            readContactsEmitter.onError(permissionRequestError);
                            readContactsEmitter = null;
                        });
    }

    public void getContacts() {
        List<Contact> contactList = new ArrayList<>();
        HashMap<Integer, Contact> contactMap = new HashMap<>();

        Cursor cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
        if(cursor!=null && cursor.getCount()>0) {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Data._ID));
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)).trim();
                String avatar = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI));
                String phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).trim();

                Contact contact = new Contact(id, name, avatar, phoneNumber);
                mergeAndAddContactToList(contactMap, contact);
            }
            cursor.close();
        }
        putDataFromMapToList(contactList, contactMap);
        Collections.sort(contactList, new Comparator<Contact>() {
            @Override
            public int compare(Contact s1, Contact s2) {
                return s1.name.compareTo(s2.name);
            }
        });
        readContactsEmitter.onNext(contactList);
        readContactsEmitter.onComplete();
    }

    private void mergeAndAddContactToList(HashMap<Integer, Contact> map, Contact contact) {
        Contact value = map.get(contact.hashCode());
        if (value != null) {
            value.merge(contact);
        } else {
            map.put(contact.hashCode(), contact);
        }
    }

    private void putDataFromMapToList(List<Contact> contactList, HashMap<Integer, Contact> map) {
        if (contactList == null) {
            contactList = new ArrayList<>();
        }
        if (map == null || map.size() == 0) {
            return;
        }
        for (Map.Entry<Integer, Contact> contactEntry : map.entrySet()) {
            contactList.add(contactEntry.getValue());
        }
    }

    public Observable<List<Call>> requestReadCallLog(final PermissionActivity currentActivity, long dateFrom, int limit) {
        Observable<List<Call>> readCallLogObservable = Observable
                .create(emitter -> {
                    readCallLogEmitter = emitter;
                    int permissionState = ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG);
                    if (permissionState != PackageManager.PERMISSION_GRANTED) {
                        requestReadCallLogPermission(currentActivity, dateFrom, limit);
                    } else {
                        getCallLog(dateFrom, limit);
                    }
                });
        return readCallLogObservable.compose(applyAsyncSchedulers());
    }

    private void requestReadCallLogPermission(PermissionActivity currentActivity, long dateFrom, int limit) {
        currentActivity
                .requestPermission(Manifest.permission.READ_CALL_LOG)
                .subscribe(
                        (PermissionRequestResult permissionRequestResult) -> {
                            if (permissionRequestResult.isGranted) {
                                getCallLog(dateFrom, limit);
                            } else {
                                readCallLogEmitter.onError(new PermissionRequestError());
                                readCallLogEmitter = null;
                            }
                        },
                        error -> {
                            PermissionRequestError permissionRequestError = new PermissionRequestError();
                            readCallLogEmitter.onError(permissionRequestError);
                            readCallLogEmitter = null;
                        });
    }

    public void getCallLog(long dateFrom, int limit) {
        List<Call> callList = new ArrayList<>();
        String selection = android.provider.CallLog.Calls.DATE + " < ?";
        String[] selectionArgs = new String[] {String.valueOf(dateFrom)};
        String strOrder = android.provider.CallLog.Calls.DATE + " DESC";

        Cursor cursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, selection, selectionArgs, strOrder);
        if(cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext() && callList.size() <= limit) {
                String id = cursor.getString(cursor.getColumnIndex(CallLog.Calls._ID));
                String number = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
                int type = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.TYPE));
                String name = cursor.getString(cursor.getColumnIndex(CallLog.Calls.CACHED_NAME));
                String cachedLookupUri = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
                        ? cursor.getString(cursor.getColumnIndex(CallLog.Calls.CACHED_LOOKUP_URI))
                        : null;
                Long date = cursor.getLong(cursor.getColumnIndex(CallLog.Calls.DATE));
                Long duration = cursor.getLong(cursor.getColumnIndex(CallLog.Calls.DURATION));
                String avatar = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                        ? cursor.getString(cursor.getColumnIndex(CallLog.Calls.CACHED_PHOTO_URI))
                        : null;

                Call call = new Call(id, number, type, name, cachedLookupUri, date, duration, avatar);
                callList.add(call);
            }
            cursor.close();
        }

        readCallLogEmitter.onNext(callList);
        readCallLogEmitter.onComplete();
    }
}
