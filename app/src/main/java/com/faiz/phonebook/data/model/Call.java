package com.faiz.phonebook.data.model;

import android.net.Uri;

import com.faiz.phonebook.R;
import com.faiz.phonebook.data.ServiceProvider;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class Call implements Serializable {

    private static final int SEC_IN_HOUR = 3600;
    private static final int SEC_IN_MIN = 60;

    public Call() {
    }

    public Call(String id, String number, int type, String name, String cachedLookupUri, Long date, Long duration, String avatar) {
        this.id = id;
        this.number = number;
        this.type = type;
        this.name = name;
        this.cachedLookupUri = cachedLookupUri;
        this.date = date;
        this.duration = duration;
        this.avatar = avatar;
    }

    private String id;
    private String number;
    private int type;
    private String name;
    private String cachedLookupUri;
    private Long date;
    private Long duration;
    private String avatar;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCachedLookupUri() {
        return cachedLookupUri;
    }

    public void setCachedLookupUri(String cachedLookupUri) {
        this.cachedLookupUri = cachedLookupUri;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getDurationString() {
        String res = "";
        if (duration > 0) {
            long hours = duration / SEC_IN_HOUR;
            long minutes = (duration % SEC_IN_HOUR) / SEC_IN_MIN;
            long sec = (duration % SEC_IN_HOUR) % SEC_IN_MIN;
            if (hours > 0) {
                res += hours + ServiceProvider.getInstance().getResourceHelper().getString(R.string.call_duration_hours);
            }
            if (minutes > 0) {
                res += minutes + ServiceProvider.getInstance().getResourceHelper().getString(R.string.call_duration_minutes);
            }
            if (sec > 0) {
                res += sec + ServiceProvider.getInstance().getResourceHelper().getString(R.string.call_duration_sec);
            }
        } else {
            res = "0" + ServiceProvider.getInstance().getResourceHelper().getString(R.string.call_duration_sec);
        }

        return res;
    }

    public String getDateString() {
        Date date = new Date(this.date);
        SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");
        return format.format(date);
    }
}
