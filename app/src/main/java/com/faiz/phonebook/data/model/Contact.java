package com.faiz.phonebook.data.model;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;

public class Contact implements Serializable {
    public String id;
    public String name;
    public String ava;

    private ArrayList<Phone> phones;

    public ArrayList<Phone> getPhones() {
        if (phones == null) {
            phones = new ArrayList<>();
        }
        return phones;
    }

    public Contact(String id, String name, String ava) {
        this.id = id;
        this.name = name;
        this.ava = ava;
        this.phones = new ArrayList<>();
    }

    public Contact(String id, String name, String ava, String phone) {
        this.id = id;
        this.name = name;
        this.ava = ava;
        this.phones = new ArrayList<>();
        if (phone != null) {
            phones.add(new Phone(id, phone));
        }
    }

    public Contact(String id, String name, String ava, ArrayList<Phone> phones) {
        this.id = id;
        this.name = name;
        this.ava = ava;
        this.phones = phones;
    }

    @Override
    public int hashCode() {
        return (name).hashCode();
    }

    @Override
    public boolean equals(Object cont) {
        if (cont == null)
            return false;
        if (cont == this)
            return true;
        if (cont.getClass() != getClass())
            return false;
        return (((Contact) cont).name.equals(name));
    }

    public String toString() {
        StringBuffer stb = new StringBuffer();
        stb.append("name:  " + name);
        stb.append("; ava:  " + ava);
        for (Phone phone : getPhones()) {
            stb.append("; phone:  " + phone.getPhoneNumber());
        }
        return stb.toString();
    }

    public void merge(Contact contact) {
        if(ava == null && contact.ava != null) {
            ava = contact.ava;
        }
        for (Phone contactPhone : contact.getPhones()) {
            boolean alreadyExist = false;
            for (Phone curPhone : getPhones()) {
                alreadyExist = TextUtils.equals(curPhone.getPhoneNumber(), contactPhone.getPhoneNumber());
                if (alreadyExist) {
                    break;
                }
            }
            if (!alreadyExist) {
                getPhones().add(contactPhone);
            }
        }
    }
}