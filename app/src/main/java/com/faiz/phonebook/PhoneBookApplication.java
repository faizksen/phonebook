package com.faiz.phonebook;

import android.app.Application;

import com.faiz.phonebook.data.ServiceProvider;

public class PhoneBookApplication extends Application {

    private static PhoneBookApplication instance;

    public static PhoneBookApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ServiceProvider.makeInstance(this);
    }
}
