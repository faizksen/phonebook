package com.faiz.phonebook.ui;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.ViewGroup;

import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.faiz.phonebook.R;
import com.faiz.phonebook.data.ServiceProvider;
import com.faiz.phonebook.ui.base.BaseModalFragment;
import com.faiz.phonebook.ui.base.PermissionActivity;
import com.faiz.phonebook.ui.main.MainFragment;

public class MainActivity extends PermissionActivity {

    private ViewGroup fragmentContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentContainer = findViewById(R.id.main_activity_fragment_container);

        showFragment(MainFragment.newInstance(), false);
    }

    @Override
    protected void onDestroy() {
        ServiceProvider.getInstance().getDatabaseService().closeRealm();
        super.onDestroy();
    }

    public void showFragment (Fragment fragment, boolean addToBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager == null || fragment == null) {
            return;
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        if (fragment instanceof BaseModalFragment) {
            fragmentTransaction.add(R.id.main_activity_modal_fragment_container, fragment);
        } else {
            fragmentTransaction.replace(R.id.main_activity_fragment_container, fragment);
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
