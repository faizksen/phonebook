package com.faiz.phonebook.ui.contact;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.faiz.phonebook.R;
import com.faiz.phonebook.data.model.Phone;

import java.util.ArrayList;

public class ContactContentAdapter extends RecyclerView.Adapter<ContactContentViewHolder> {

    private ArrayList<Phone> items;

    public void setItems(ArrayList<Phone> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ContactContentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ContactContentViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_content, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ContactContentViewHolder holder, int position) {
        Phone content = getItem(position);
        setContent(content, holder);
    }

    private void setContent (Phone phone, ContactContentViewHolder holder) {
        String phoneNumber = phone.getPhoneNumber();
        if (phoneNumber != null && !phoneNumber.trim().isEmpty()) {
            holder.phone.setText(phoneNumber);
            Drawable img = ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.ic_call);
            holder.phone.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            holder.messageButton.setVisibility(View.VISIBLE);
        } else {
            holder.phone.setText("unknown type");
            holder.phone.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            holder.messageButton.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public Phone getItem (int position) {
        if (items == null || items.size() <= position) {
            return null;
        } else {
            return items.get(position);
        }
    }
}
