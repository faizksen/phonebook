package com.faiz.phonebook.ui.contact;

import com.faiz.phonebook.data.model.Phone;
import com.faiz.phonebook.ui.base.MvpView;

import java.util.ArrayList;

public interface ContactMvpView extends MvpView {
    String ARG_CONTACT = "arg_contact_id";
    String SAVED_CONTACT = "saved_contact";

    void setContentItems(ArrayList<Phone> phones);
}
