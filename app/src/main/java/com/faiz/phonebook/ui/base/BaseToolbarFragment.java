package com.faiz.phonebook.ui.base;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;

import com.faiz.phonebook.R;
import com.faiz.phonebook.data.ServiceProvider;
import com.google.android.material.appbar.AppBarLayout;

public abstract class BaseToolbarFragment extends BaseFragment implements BaseToolbarMvpView {

    private AppBarLayout appBarLayout;
    private ActionBar toolbar;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        appBarLayout = view.findViewById(R.id.appbar);
        if (appBarLayout == null) {
            return;
        }
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        getMainActivity().setSupportActionBar(toolbar);
        this.toolbar = getMainActivity().getSupportActionBar();
        if (setUpMenuItems() != -1) {
            setHasOptionsMenu(true);
        }
    }

    protected void setTitle(String title) {
        if (toolbar != null) {
            toolbar.setTitle(title);
        }
    }

    protected void setTitle(int resIdTitle) {
        if (toolbar != null) {
            toolbar.setTitle(ServiceProvider.getInstance().getResourceHelper().getString(resIdTitle));
        }
    }

    protected void setDisplayHomeAsUpEnabled (boolean isEnabled) {
        if (toolbar != null) {
            toolbar.setDisplayHomeAsUpEnabled(isEnabled);
        }
    }

    protected int setUpMenuItems() {
        return -1;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (toolbar != null && setUpMenuItems() != -1) {
            inflater.inflate(setUpMenuItems(), menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
