package com.faiz.phonebook.ui.base;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.faiz.phonebook.R;
import com.faiz.phonebook.data.ServiceProvider;
import com.faiz.phonebook.ui.MainActivity;

public abstract class BaseFragment extends Fragment implements MvpView {

    protected Dialog progressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            Bundle savedData = ServiceProvider.getInstance().getDatabaseService().getState();
            if (savedData != null) {
                savedInstanceState.putAll(savedData);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResId(), container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        BasePresenter presenter = getPresenter();
        if (presenter != null) {
            presenter.detachView();
        }
        hideProgressbar();
        progressBar = null;
        super.onDestroyView();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        getPresenter().onSaveInstanceState(outState);
        Bundle outData = new Bundle();
        getPresenter().onSaveInstanceData(outData);
        ServiceProvider.getInstance().getDatabaseService().saveState(outData);
//        outState.putAll(outData);
        super.onSaveInstanceState(outState);
    }

    protected abstract @LayoutRes
    int getLayoutResId();

    public abstract BasePresenter getPresenter();

    public void showFragment(Fragment fragment) {
        MainActivity activity = getMainActivity();
        if (activity != null) {
            activity.showFragment(fragment, true);
        }
    }

    public MainActivity getMainActivity () {
        Activity activity = getActivity();
        if (activity instanceof MainActivity) {
            return (MainActivity) activity;
        }
        return null;
    }

    @Override
    public void finish() {
        finish(false, null);
    }

    @Override
    public void finish (boolean isSuccess, Intent resultIntent) {
        MainActivity activity = getMainActivity();
        if (activity != null) {
            Fragment targetFragment = getTargetFragment();
            if (targetFragment instanceof BaseFragment
                    && ((BaseFragment) targetFragment).getPresenter().isViewAttached()
                    && getPresenter().getRequestCode() != 0) {
                targetFragment.onActivityResult(getPresenter().getRequestCode(), isSuccess ? Activity.RESULT_OK : Activity.RESULT_CANCELED, resultIntent);
            }
            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            if (fragmentManager != null) {
                try {
                    fragmentManager.popBackStackImmediate();
                } catch (IllegalStateException e) {
                    fragmentManager.popBackStack();
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void showProgressBar() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getMainActivity());
        builder.setView(R.layout.progress);
        progressBar = builder.create();
        progressBar.show();
    }

    @Override
    public void hideProgressbar() {
        if (progressBar != null) {
            progressBar.dismiss();
        }
    }


}
