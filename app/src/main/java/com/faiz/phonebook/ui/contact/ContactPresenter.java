package com.faiz.phonebook.ui.contact;

import android.os.Bundle;

import com.faiz.phonebook.R;
import com.faiz.phonebook.data.model.Contact;
import com.faiz.phonebook.data.model.Phone;
import com.faiz.phonebook.ui.base.BasePresenter;

import java.util.ArrayList;

import static com.faiz.phonebook.ui.contact.ContactMvpView.SAVED_CONTACT;

public class ContactPresenter extends BasePresenter<ContactMvpView> {

    private Contact contact;
    private ArrayList<Phone> content;

    @Override
    public void attachView(ContactMvpView mvpView, Bundle savedInstanceState) {
        super.attachView(mvpView, savedInstanceState);
        if (savedInstanceState != null) {
            contact = (Contact) savedInstanceState.getSerializable(SAVED_CONTACT);
        }

        if (contact != null) {
            content = contact.getPhones();
        } else {
            content = new ArrayList<>();
        }
        getMvpView().setContentItems(content);
    }

    @Override
    public void onSaveInstanceData(Bundle outData) {
        outData.putSerializable(SAVED_CONTACT, contact);
        super.onSaveInstanceData(outData);
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public String getAvatar() {
        return contact == null ? null : contact.ava;
    }

    public String getName() {
        return contact == null ? null : contact.name;
    }

    public void onContactPressed(int position) {
        //ToDo: phone call
    }

    public void onMenuItemPressed(int itemId) {
        switch (itemId) {
            case R.id.action_share:
                //ToDo: share()
                break;
            case R.id.action_fav:
                //ToDo: add/del to Favourites
                break;
        }
    }
}
