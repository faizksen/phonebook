package com.faiz.phonebook.ui.main.pager;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.faiz.phonebook.R;

class HeaderMainViewHolder extends RecyclerView.ViewHolder {

    public TextView header;

    public HeaderMainViewHolder(@NonNull View itemView) {
        super(itemView);

        header = itemView.findViewById(R.id.header);
    }
}
