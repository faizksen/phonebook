package com.faiz.phonebook.ui.createContact;

import com.faiz.phonebook.R;
import com.faiz.phonebook.ui.base.BasePresenter;
import com.faiz.phonebook.ui.base.BaseToolbarFragment;

public class CreateContactFragment extends BaseToolbarFragment {

    private CreateContactPresenter presenter = new CreateContactPresenter();

    public static CreateContactFragment newInstance() {
        return new CreateContactFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_test2;
    }

    @Override
    public BasePresenter getPresenter() {
        return presenter;
    }
}
