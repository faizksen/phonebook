package com.faiz.phonebook.ui.base;

import android.content.pm.PackageManager;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.faiz.phonebook.util.PermissionRequestResult;

import io.reactivex.Emitter;
import io.reactivex.Observable;

public abstract class PermissionActivity extends AppCompatActivity {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private Emitter<? super PermissionRequestResult> permissionResultSubscriber;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0 && permissions.length != 0) {
            for (int i = 0; i < permissions.length; i++) {
                handlePermissionStatus(permissions[i], grantResults[i]);
            }
        }
    }

    private void handlePermissionStatus(String permission, int grantResult) {
        if (permissionResultSubscriber == null) {
            return;
        }
        switch (grantResult) {
            case PackageManager.PERMISSION_GRANTED:
                permissionResultSubscriber.onNext(new PermissionRequestResult(true));
                break;
            case PackageManager.PERMISSION_DENIED:
                permissionResultSubscriber.onNext(new PermissionRequestResult(false));
                break;
        }
    }

    public Observable<PermissionRequestResult> requestPermission(String permission){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Observable<PermissionRequestResult> permissionObservable = Observable
                    .create(
                            emitter -> {
                                permissionResultSubscriber = emitter;
                            }
                    );
            requestPermissions(new String[] {permission}, 0);
            return permissionObservable;
        } else {
            return Observable.just(new PermissionRequestResult(true));
        }
    }

}
