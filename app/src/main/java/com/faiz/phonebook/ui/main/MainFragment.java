package com.faiz.phonebook.ui.main;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.faiz.phonebook.R;
import com.faiz.phonebook.ui.base.BaseFragment;
import com.faiz.phonebook.ui.base.BasePresenter;
import com.faiz.phonebook.ui.main.pager.MainPageFragment;
import com.google.android.material.tabs.TabLayout;

public class MainFragment extends BaseFragment implements MainMvpView {

    private MainPresenter presenter = new MainPresenter();

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private MainPagerAdapter viewpagerAdapter;

    public static MainFragment newInstance () {
        return new MainFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tabLayout = view.findViewById(R.id.main_tab_layout);
        viewPager = view.findViewById(R.id.main_view_pager);

        tabLayout.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.white));
        tabLayout.setTabTextColors(R.color.main_disabled, R.color.main_blue);
        tabLayout.setupWithViewPager(viewPager);

        if (viewpagerAdapter == null) {
            viewpagerAdapter = new MainPagerAdapter(getChildFragmentManager());
        }

        viewPager.setAdapter(viewpagerAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                presenter.onPageSelected(position);

                MainPageFragment tabFragment = getTabFragment(position);
                if (tabFragment != null) {
                    tabFragment.onPageSelected();
                } else { //at first call fragments in adapter are not instantiated yet
                    viewpagerAdapter.pendingSelectedItem = position;
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }

        });

        presenter.attachView(this, savedInstanceState);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_main;
    }

    @Override
    public BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void setTabPosition(int selectedTabPosition) {
        if (viewPager != null) {
            viewPager.setCurrentItem(selectedTabPosition);
        }
    }

    public boolean isTabFragmentVisible (int position) {
        return presenter.getCurrentSelectedTabPosition() == position;
    }

    public MainPageFragment getTabFragment(int tabPosition) {
        return (MainPageFragment) viewpagerAdapter.getRegisteredFragment(tabPosition);
    }

    private class MainPagerAdapter extends FragmentStatePagerAdapter {
        public int pendingSelectedItem = -1;
        SparseArray<Fragment> registeredFragments = new SparseArray<>();

        public MainPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return new MainPageFragment().newInstance(position);
        }


        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case TAB_POSITION_FAVOURITES:
                    return getResources().getString(R.string.main_tab_title_favourites);
                case TAB_POSITION_RECENT:
                    return getResources().getString(R.string.main_tab_title_recent);
                case TAB_POSITION_CONTACTS:
                    return getResources().getString(R.string.main_tab_title_contacts);
                default:
                    return "";
            }
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            if (pendingSelectedItem == position) {
                pendingSelectedItem = -1;
                ((MainPageFragment) fragment).onPageSelected();
            }
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }
    }
}
