package com.faiz.phonebook.ui.main.pager;

import com.faiz.phonebook.data.model.BaseObject;
import com.faiz.phonebook.data.model.Call;
import com.faiz.phonebook.data.model.Contact;
import com.faiz.phonebook.util.MultiTypeAdapter;

import java.util.ArrayList;

import static com.faiz.phonebook.ui.main.pager.MainPageMvpView.TYPE_CALL;
import static com.faiz.phonebook.ui.main.pager.MainPageMvpView.TYPE_CONTACT;
import static com.faiz.phonebook.ui.main.pager.MainPageMvpView.TYPE_CREATE_CONTACT;
import static com.faiz.phonebook.ui.main.pager.MainPageMvpView.TYPE_HEADER;

public class MainPageListAdapter extends MultiTypeAdapter {

    private ArrayList<Object> items = new ArrayList<>();

    public void setItems(ArrayList<Object> items) {
        this.items = items;
    }

    @Override
    public Object getItem(int position) {
        if (items.isEmpty() || position > items.size()) {
            return null;
        }
        return items.get(position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof Contact) {
            return TYPE_CONTACT;
        } else if (item instanceof String) {
            return TYPE_HEADER;
        } else if (item instanceof Call) {
            return TYPE_CALL;
        } else if (item instanceof BaseObject){
            return TYPE_CREATE_CONTACT;
        }
        return -1;
    }
}
