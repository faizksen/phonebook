package com.faiz.phonebook.ui.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import com.faiz.phonebook.data.ResourceHelper;
import com.faiz.phonebook.data.ServiceProvider;
import com.faiz.phonebook.ui.MainActivity;

import io.reactivex.disposables.CompositeDisposable;

public class BasePresenter<T extends MvpView> implements Presenter<T> {

    protected T mvpView;

    protected ResourceHelper resourceHelper;

    protected CompositeDisposable pendingRequestsDisposable = new CompositeDisposable();
    private int requestCode;

    public BasePresenter() {
        resourceHelper = ServiceProvider.getInstance().getResourceHelper();
    }

    @Override
    public void attachView(T mvpView, Bundle savedInstanceState) {
        this.mvpView = mvpView;
    }

    @Override
    public void detachView() {
        mvpView = null;
        if (pendingRequestsDisposable != null) {
            pendingRequestsDisposable.clear();
        }
    }

    public T getMvpView() {
        return mvpView;
    }

    public boolean isViewAttached() {
        return mvpView != null;
    }

    public void onSaveInstanceState (Bundle outState) {

    }

    public void onSaveInstanceData(Bundle outData) {

    }

    public void showFragment(Fragment fragment) {
        showFragmentForResult(fragment, 0);
    }

    public void showFragmentForResult (Fragment fragment, int requestCode) {
        if (getMvpView() instanceof BaseFragment) {
            if (requestCode != 0) {
                BaseFragment targetFragment = (BaseFragment) getMvpView();
                fragment.setTargetFragment(targetFragment, requestCode);
            }
            getMvpView().showFragment(fragment);
        }
    }

    public int getRequestCode() {
        return requestCode;
    }
}
