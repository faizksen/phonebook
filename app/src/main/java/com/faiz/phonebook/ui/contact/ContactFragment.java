package com.faiz.phonebook.ui.contact;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.faiz.phonebook.R;
import com.faiz.phonebook.data.ServiceProvider;
import com.faiz.phonebook.data.model.Contact;
import com.faiz.phonebook.data.model.Phone;
import com.faiz.phonebook.ui.base.BaseCollapsingToolbarFragment;
import com.faiz.phonebook.ui.base.BasePresenter;
import com.faiz.phonebook.util.ItemClickSupport;

import java.util.ArrayList;

public class ContactFragment extends BaseCollapsingToolbarFragment implements ContactMvpView {

    private ContactPresenter presenter = new ContactPresenter();

    private RecyclerView recyclerView;
    private ContactContentAdapter adapter;

    public static ContactFragment newInstance (Contact contact) {
        ContactFragment fragment = new ContactFragment();
        Bundle arg = new Bundle();
        arg.putSerializable(ARG_CONTACT, contact);
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_contact;
    }

    @Override
    public BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle arg = getArguments();
        if (arg != null) {
            Contact contact = (Contact) arg.getSerializable(ARG_CONTACT);
            presenter.setContact(contact);
        }

        recyclerView = view.findViewById(R.id.phones_recycler);
        adapter = new ContactContentAdapter();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext(), RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(adapter);
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(
                (recyclerView, position, v) -> presenter.onContactPressed(position)
        );

        initToolbar();

        presenter.attachView(this, savedInstanceState);
    }

    private void initToolbar() {
        String avatar = presenter.getAvatar();
        if (avatar != null) {
            loadBackgroundImage(avatar);
        } else {
            loadBackgroundImage(ContextCompat.getDrawable(ServiceProvider.getInstance().getApplicationContext(), R.drawable.ic_account_circle));
        }
        String name = presenter.getName();
        setTitle(name == null ? "" : name);
        setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected int setUpMenuItems() {
        return R.menu.app_bar_menu_contact_fragment;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        presenter.onMenuItemPressed(item.getItemId());
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setContentItems(ArrayList<Phone> phones) {
        if (adapter != null) {
            adapter.setItems(phones);
            adapter.notifyDataSetChanged();
        }
    }
}
