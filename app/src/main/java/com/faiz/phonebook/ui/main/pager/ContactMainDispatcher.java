package com.faiz.phonebook.ui.main.pager;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.faiz.phonebook.R;
import com.faiz.phonebook.data.ServiceProvider;
import com.faiz.phonebook.data.model.Contact;
import com.faiz.phonebook.util.BaseAdapterDispatcher;

import java.util.List;

class ContactMainDispatcher extends BaseAdapterDispatcher<ContactViewHolder, Contact> {

    private MainPageListAdapter adapter;

    public ContactMainDispatcher (MainPageListAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_contact, parent, false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, Contact contact, int position, List payloads) {
        setAvatar(holder, contact);
        setName(holder, contact);
        setFirstSymbol(holder, contact, position);
    }

    private void setAvatar (ContactViewHolder holder, Contact contact) {
        if (contact.ava == null || contact.ava.isEmpty()) {
            holder.avatar.setImageDrawable(ContextCompat.getDrawable(holder.avatar.getContext(), R.drawable.ic_account_circle));
        } else {
            Glide.with(holder.avatar.getContext())
                    .load(contact.ava)
                    .apply(RequestOptions
                            .centerCropTransform()
                            .error(R.drawable.ic_account_circle))
                    .into(holder.avatar);
        }
    }

    private void setName (ContactViewHolder holder, Contact contact) {
        String name = "";

        if (contact.name != null) {
            name = contact.name;
        }
        holder.contactName.setText(name);
    }

    private String getFirstSymbol (Contact contact) {
        if (contact.name == null || contact.name.isEmpty()) {
            return ServiceProvider.getInstance().getResourceHelper().getString(R.string.octothorpe);
        } else {
            char firstSymbol = contact.name.charAt(0);
            if (Character.isDigit(firstSymbol)) {
                return ServiceProvider.getInstance().getResourceHelper().getString(R.string.octothorpe);
            } else if (Character.isLetter(firstSymbol)) {
                return String.valueOf(firstSymbol);
            } else {
                return ServiceProvider.getInstance().getResourceHelper().getString(R.string.ellipsis);
            }
        }
    }

    private void setFirstSymbol (ContactViewHolder holder, Contact contact, int position) {
        boolean isVisibleFirstLetter = true;
        String currentFirstSymbol = getFirstSymbol(contact);
        if (position > 1) {
            Object previousItem = adapter.getItem(position-1);
            if (previousItem instanceof Contact && TextUtils.equals(currentFirstSymbol, getFirstSymbol((Contact) previousItem))) {
                isVisibleFirstLetter = false;
            }
        }
        if (isVisibleFirstLetter) {
            holder.firstLetter.setText(currentFirstSymbol);
            holder.firstLetter.setVisibility(View.VISIBLE);
        } else {
            holder.firstLetter.setVisibility(View.GONE);
        }
    }
}
