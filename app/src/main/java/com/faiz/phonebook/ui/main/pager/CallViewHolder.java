package com.faiz.phonebook.ui.main.pager;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.faiz.phonebook.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class CallViewHolder extends RecyclerView.ViewHolder {

    public CircleImageView avatar;
    public TextView name;
    public TextView info;
    public ImageButton recallButton;

    public CallViewHolder(@NonNull View itemView) {
        super(itemView);

        avatar = itemView.findViewById(R.id.contact_avatar);
        name = itemView.findViewById(R.id.contact_name);
        info = itemView.findViewById(R.id.call_info);
        recallButton = itemView.findViewById(R.id.call_button);
    }
}
