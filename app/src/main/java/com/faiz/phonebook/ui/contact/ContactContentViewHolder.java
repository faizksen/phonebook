package com.faiz.phonebook.ui.contact;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.faiz.phonebook.R;

public class ContactContentViewHolder extends RecyclerView.ViewHolder {

    public TextView phone;
    public ImageButton messageButton;

    public ContactContentViewHolder(@NonNull View view) {
        super(view);

        phone = view.findViewById(R.id.phone);
        messageButton = view.findViewById(R.id.message_button);
    }
}
