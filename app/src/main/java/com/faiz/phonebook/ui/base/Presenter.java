package com.faiz.phonebook.ui.base;

import android.os.Bundle;

public interface Presenter<T extends MvpView> {

    void attachView(T mvpView, Bundle savedInstanceState);

    void detachView();
}
