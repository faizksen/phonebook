package com.faiz.phonebook.ui.main.pager;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.faiz.phonebook.R;

import de.hdodenhof.circleimageview.CircleImageView;

class ContactViewHolder extends RecyclerView.ViewHolder {

    public TextView firstLetter;
    public CircleImageView avatar;
    public TextView contactName;

    public ContactViewHolder(View view) {
        super(view);

        firstLetter = view.findViewById(R.id.first_letter);
        avatar = view.findViewById(R.id.contact_avatar);
        contactName = view.findViewById(R.id.contact_name);
    }
}
