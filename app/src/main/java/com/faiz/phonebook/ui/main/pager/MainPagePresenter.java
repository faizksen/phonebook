package com.faiz.phonebook.ui.main.pager;

import android.os.Bundle;

import com.faiz.phonebook.R;
import com.faiz.phonebook.data.AppModel;
import com.faiz.phonebook.data.ServiceProvider;
import com.faiz.phonebook.data.model.BaseObject;
import com.faiz.phonebook.data.model.Call;
import com.faiz.phonebook.data.model.Contact;
import com.faiz.phonebook.ui.MainActivity;
import com.faiz.phonebook.ui.base.BasePresenter;
import com.faiz.phonebook.ui.contact.ContactFragment;
import com.faiz.phonebook.ui.createContact.CreateContactFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.disposables.Disposable;

import static com.faiz.phonebook.ui.main.MainMvpView.TAB_POSITION_CONTACTS;
import static com.faiz.phonebook.ui.main.MainMvpView.TAB_POSITION_FAVOURITES;
import static com.faiz.phonebook.ui.main.MainMvpView.TAB_POSITION_RECENT;

public class MainPagePresenter extends BasePresenter<MainPageMvpView> {

    private static final int CALL_PER_PAGE = 100;
    private static final int TODAY_CALL = 1;
    private static final int YESTERDAY_CALL = 2;
    private static final int PREVIOUS_CALL = 3;

    private static final String SAVED_ITEMS = "saved_items";
    private static final String SAVED_TYPE = "saved_type";
    private int type;
    private ArrayList<Object> items;
    private boolean selectOnAttach = false;

    private AppModel appModel;

    public MainPagePresenter () {
        appModel = ServiceProvider.getInstance().getAppModel();
    }

    @Override
    public void attachView(MainPageMvpView mvpView, Bundle savedInstanceState) {
        super.attachView(mvpView, savedInstanceState);
        if (savedInstanceState != null) {
            items = (ArrayList<Object>) savedInstanceState.getSerializable(SAVED_ITEMS);
            type = savedInstanceState.getInt(SAVED_TYPE);
            if (items == null || items.isEmpty()) {
                performGetItems();
            } else {
                getMvpView().setItems(items);
            }
        } else {
            performGetItems();
        }

        if (selectOnAttach) {
            selectOnAttach = false;
            onPageSelected();
        }
    }

    @Override
    public void onSaveInstanceData(Bundle outData) {
        outData.putInt(SAVED_TYPE, type);
        outData.putSerializable(SAVED_ITEMS, items);
        super.onSaveInstanceData(outData);
    }

    public void setType (int type) {
        this.type = type;
    }

    public void onPageSelected() {
        //called when fragment becomes active in view pager
        if (!isViewAttached()) {
            selectOnAttach = true;
            return;
        }
        if (items == null || items.isEmpty()) {
            performGetItems();
        } else {
            getMvpView().setItems(items);
        }
    }

    private void performGetItems () {
        switch (type) {
            case TAB_POSITION_FAVOURITES:
                performGetFavouritesContacts();
                break;
            case TAB_POSITION_RECENT:
                performGetCalls();
                break;
            case TAB_POSITION_CONTACTS:
                performGetContacts();
                break;
        }
    }

    private void performGetFavouritesContacts() {

    }

    private void performGetCalls() {
        if (getMvpView() == null) {
            return;
        }
        getMvpView().setListLoading(true);

        MainActivity activity = getMvpView().getMainActivity();
        if (activity == null) {
            return;
        }
        if (items == null) {
            items = new ArrayList<>();
        }
        long dateFrom = items.size() == 0
                ? Calendar.getInstance().getTimeInMillis()
                : ((Call) items.get(items.size() - 1)).getDate();

        Disposable callsSub = appModel
                .requestReadCallLog(activity, dateFrom, CALL_PER_PAGE)
                .subscribe(
                        (List<Call> calls) -> {
                            if (items == null) {
                                items = new ArrayList<>();
                            }
                            if (calls == null) {
                                calls = new ArrayList<>();
                            }
                            if (!calls.isEmpty()) {
                                int insertPosition = items.size();
                                processCalls(calls);
                                if (insertPosition == 0) {
                                    getMvpView().setItems(items);
                                } else {
                                    getMvpView().addItems(insertPosition, calls.size());
                                }
                            }
                            getMvpView().setEndlessModeEnabled(!calls.isEmpty());
                            getMvpView().setListLoading(false);
                        },
                        throwable -> {
                            getMvpView().setListLoading(false);
                            getMvpView().showNoPermissionAlertDialog();
                        }
                );
        pendingRequestsDisposable.add(callsSub);
    }

    private void processCalls(List<Call> calls) {
        Calendar now = Calendar.getInstance();
        Calendar callTime = Calendar.getInstance();
        boolean isTodayHeaderInserted = false;
        boolean isYesterdayHeaderInserted = false;
        boolean isAnotherDayHeaderInserted = false;
        if (items.size() > 0) {
            Object lastCall = items.get(items.size()-1);
            if (lastCall instanceof Call) {
                callTime.setTimeInMillis(((Call) lastCall).getDate());
                switch (getCallType(now, callTime)) {
                    case TODAY_CALL:
                        isTodayHeaderInserted = true;
                        break;
                    case YESTERDAY_CALL:
                        isYesterdayHeaderInserted = true;
                        break;
                    case PREVIOUS_CALL:
                        isAnotherDayHeaderInserted = true;
                        items.addAll(calls);
                        break;
                }
            }
        }
        if (!isAnotherDayHeaderInserted) {
            for (int i = 0; i < calls.size(); i++) {
                callTime.setTimeInMillis(calls.get(i).getDate());
                switch (getCallType(now, callTime)) {
                    case TODAY_CALL:
                        if (!isTodayHeaderInserted) {
                            isTodayHeaderInserted = true;
                            items.add(ServiceProvider.getInstance().getResourceHelper().getString(R.string.today));
                        }
                        break;
                    case YESTERDAY_CALL:
                        if (!isYesterdayHeaderInserted) {
                            isYesterdayHeaderInserted = true;
                            items.add(ServiceProvider.getInstance().getResourceHelper().getString(R.string.yesterday));
                        }
                        break;
                    case PREVIOUS_CALL:
                        isAnotherDayHeaderInserted = true;
                        items.add(ServiceProvider.getInstance().getResourceHelper().getString(R.string.previous));
                        items.addAll(calls.subList(i, calls.size()));
                        break;
                }
                if (isAnotherDayHeaderInserted) {
                    break;
                }
                items.add(calls.get(i));
            }
        }
    }

    private int getCallType (Calendar day1, Calendar day2) {
        int res = -1;
        if (isToday(day1, day2)) {
            res = TODAY_CALL;
        } else if (isYesterday(day1, day2)) {
            res = YESTERDAY_CALL;
        } else if (day1.getTimeInMillis() > day2.getTimeInMillis()) {
            res = PREVIOUS_CALL;
        }
        return res;
    }

    private boolean isToday (Calendar day1, Calendar day2) {
        return day1.get(Calendar.YEAR) == day2.get(Calendar.YEAR)
                && day1.get(Calendar.MONTH) == day2.get(Calendar.MONTH)
                && day1.get(Calendar.DAY_OF_MONTH) == day2.get(Calendar.DAY_OF_MONTH);
    }

    private boolean isYesterday (Calendar day1, Calendar day2) {
        return  (day1.get(Calendar.YEAR) == day2.get(Calendar.YEAR)
                && day1.get(Calendar.MONTH) == day2.get(Calendar.MONTH)
                && day1.get(Calendar.DAY_OF_MONTH) - day2.get(Calendar.DAY_OF_MONTH) == 1)
            || (day1.get(Calendar.YEAR) == day2.get(Calendar.YEAR)
                && day1.get(Calendar.MONTH) - day2.get(Calendar.MONTH) == 1
                && day1.get(Calendar.DAY_OF_MONTH) == day1.getActualMinimum(Calendar.DAY_OF_MONTH)
                && day2.get(Calendar.DAY_OF_MONTH) == day2.getActualMaximum(Calendar.DAY_OF_MONTH))
            || (day1.get(Calendar.YEAR) - day2.get(Calendar.YEAR) == 1
                && day1.get(Calendar.MONTH) == Calendar.JANUARY
                && day2.get(Calendar.MONTH) == Calendar.DECEMBER
                && day1.get(Calendar.DAY_OF_MONTH) == 1
                && day2.get(Calendar.DAY_OF_MONTH) == 31);
    }

    private void performGetContacts() {
        if (getMvpView() == null) {
            return;
        }
        MainActivity activity = getMvpView().getMainActivity();
        if (activity == null) {
            return;
        }
        if (items == null || items.isEmpty()) {
            Disposable contactSub = appModel
                    .requestReadContacts(activity)
                    .subscribe(
                            contacts -> {
                                if (items == null) {
                                    items = new ArrayList<>();
                                }
                                if (items.isEmpty()) {
                                    items.add(new BaseObject());
                                }
                                if (!contacts.isEmpty()) {
                                    items.addAll(contacts);
                                }
                                getMvpView().setItems(items);
                            },
                            throwable -> {
                                getMvpView().showNoPermissionAlertDialog();
                            }
                    );
            pendingRequestsDisposable.add(contactSub);
        }
    }

    public void onItemClicked(int position) {
        if (position < items.size()) {
            Object item = items.get(position);
            if (item instanceof Contact) {
                showFragment(ContactFragment.newInstance((Contact) item));
            } else {
                showFragment(CreateContactFragment.newInstance());
            }
        }

    }

    public void onRecallButtonPressed(Call call) {
        //ToDo: call
    }

    public void onListEndReached() {
        performGetItems();
    }
}
