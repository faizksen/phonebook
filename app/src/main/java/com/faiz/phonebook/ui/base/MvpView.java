package com.faiz.phonebook.ui.base;

import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.faiz.phonebook.ui.MainActivity;

public interface MvpView {
    MainActivity getMainActivity();

    void showProgressBar();
    void hideProgressbar();

    void showFragment (Fragment fragment);

    void finish();

    void finish(boolean isSuccess, Intent resultIntent);
}
