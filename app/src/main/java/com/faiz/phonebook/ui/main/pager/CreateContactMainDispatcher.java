package com.faiz.phonebook.ui.main.pager;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.faiz.phonebook.R;
import com.faiz.phonebook.util.BaseAdapterDispatcher;

import java.util.List;

class CreateContactMainDispatcher extends BaseAdapterDispatcher {
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new CreateContactViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_create_contact, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, Object object, int position, List payloads) {

    }
}
