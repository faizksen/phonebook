package com.faiz.phonebook.ui.main.pager;

import com.faiz.phonebook.ui.base.MvpView;

import java.util.ArrayList;

public interface MainPageMvpView extends MvpView {

    int FRAGMENT_TYPE_COUNT = 3;
    String ARG_TAB_POSITION = "arg_tab_position";

    int TYPE_CREATE_CONTACT = 0;
    int TYPE_CONTACT = 1;
    int TYPE_HEADER = 2;
    int TYPE_CALL = 3;
    int TYPE_FAVOURITE_CONTACTS = 4;

    void setItems(ArrayList<Object> items);

    void addItems(int position, int size);

    void showNoPermissionAlertDialog();

    void setListLoading(boolean isLoading);

    void setEndlessModeEnabled(boolean isEnabled);

    boolean isFragmentVisible(int position);
}
