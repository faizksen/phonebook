package com.faiz.phonebook.ui.main.pager;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.faiz.phonebook.R;
import com.faiz.phonebook.util.BaseAdapterDispatcher;

import java.util.List;

class HeaderMainDispatcher extends BaseAdapterDispatcher<HeaderMainViewHolder, String> {
    @Override
    public HeaderMainViewHolder onCreateViewHolder(ViewGroup parent) {
        return new HeaderMainViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_header, parent, false));
    }

    @Override
    public void onBindViewHolder(HeaderMainViewHolder holder, String text, int position, List<Object> payloads) {
        holder.header.setText(text);
    }
}
