package com.faiz.phonebook.ui.main;

import com.faiz.phonebook.ui.base.MvpView;

public interface MainMvpView extends MvpView {

    String SAVED_CURRENT_TAB_POSITION = "saved_current_tab_position";

    int TAB_POSITION_FAVOURITES = 0;
    int TAB_POSITION_RECENT = 1;
    int TAB_POSITION_CONTACTS = 2;

    void setTabPosition(int selectedTabPosition);
}
