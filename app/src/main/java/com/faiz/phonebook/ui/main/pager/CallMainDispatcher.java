package com.faiz.phonebook.ui.main.pager;

import android.graphics.drawable.Drawable;
import android.provider.CallLog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.faiz.phonebook.R;
import com.faiz.phonebook.data.ServiceProvider;
import com.faiz.phonebook.data.model.Call;
import com.faiz.phonebook.util.BaseAdapterDispatcher;

import java.util.Calendar;
import java.util.List;

class CallMainDispatcher extends BaseAdapterDispatcher<CallViewHolder, Call> {

    public interface EventListener {
        public void onRecallButtonPressed(Call call);
    }

    private EventListener eventistener;

    public CallMainDispatcher (EventListener eventistener) {
        this.eventistener = eventistener;
    }

    @Override
    public CallViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_call, parent, false);
        return new CallViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CallViewHolder holder, Call call, int position, List<Object> payloads) {
        setAvatar(holder, call);
        setName(holder, call);

        holder.recallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eventistener != null) {
                    eventistener.onRecallButtonPressed(call);
                }
            }
        });
    }

    private void setAvatar (CallViewHolder holder, Call call) {
        if (call.getAvatar() == null || call.getAvatar().isEmpty()) {
            holder.avatar.setImageDrawable(ContextCompat.getDrawable(holder.avatar.getContext(), R.drawable.ic_account_circle));
        } else {
            Glide.with(holder.avatar.getContext())
                    .load(call.getAvatar())
                    .apply(RequestOptions
                            .fitCenterTransform()
                            .error(R.drawable.ic_account_circle))
                    .into(holder.avatar);
        }
    }

    private void setName (CallViewHolder holder, Call call) {
        Drawable img = null;
        if (call.getType() == CallLog.Calls.MISSED_TYPE) {
            img = ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.ic_call_missed);
        } else if (call.getType() == CallLog.Calls.INCOMING_TYPE
                    || call.getType() == CallLog.Calls.BLOCKED_TYPE
                    || call.getType() == CallLog.Calls.REJECTED_TYPE) {
            img = ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.ic_call_incoming);
        } else if (call.getType() == CallLog.Calls.OUTGOING_TYPE) {
            img = ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.ic_call_outgoing);
        }
        holder.name.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);

        String name, info;
        Calendar callTime = Calendar.getInstance();
        callTime.setTimeInMillis(call.getDate());
        if (call.getName() != null && !call.getName().isEmpty()) {
            name = call.getName();
            info = String.format(ServiceProvider.getInstance().getResourceHelper().getString(R.string.call_info),
                    call.getNumber(), call.getDateString(), call.getDurationString());
        } else if (call.getNumber() != null) {
            name = call.getNumber();
            info = String.format(ServiceProvider.getInstance().getResourceHelper().getString(R.string.call_info_unknown),
                    call.getDateString(), call.getDurationString());
        } else {
            name = ServiceProvider.getInstance().getResourceHelper().getString(R.string.call_unknown);
            info = String.format(ServiceProvider.getInstance().getResourceHelper().getString(R.string.call_info_unknown),
                    call.getDateString(), call.getDurationString());
        }
        holder.name.setText(name);
        holder.info.setText(info);
    }
}
