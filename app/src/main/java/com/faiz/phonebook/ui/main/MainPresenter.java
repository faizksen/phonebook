package com.faiz.phonebook.ui.main;

import android.os.Bundle;

import com.faiz.phonebook.ui.base.BasePresenter;

import static com.faiz.phonebook.ui.main.MainMvpView.TAB_POSITION_CONTACTS;
import static com.faiz.phonebook.ui.main.MainMvpView.TAB_POSITION_RECENT;

public class MainPresenter extends BasePresenter<MainMvpView> {

    private static final String SAVED_SELECTED_TAB = "saved_selected_tab";

    private int currentSelectedTabPosition = -1;

    @Override
    public void attachView(MainMvpView mvpView, Bundle savedInstanceState) {
        super.attachView(mvpView, savedInstanceState);

        if (savedInstanceState != null) {
            currentSelectedTabPosition = savedInstanceState.getInt(SAVED_SELECTED_TAB);
        }

        if (currentSelectedTabPosition == -1) {
            currentSelectedTabPosition = TAB_POSITION_CONTACTS;
        }
        getMvpView().setTabPosition(currentSelectedTabPosition);
    }

    @Override
    public void onSaveInstanceData(Bundle outData) {
        outData.putInt(SAVED_SELECTED_TAB, currentSelectedTabPosition);
        super.onSaveInstanceData(outData);
    }

    public void onPageSelected(int position) {
        currentSelectedTabPosition = position;
    }

    public int getCurrentSelectedTabPosition () {
        return currentSelectedTabPosition;
    }
}
