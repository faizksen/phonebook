package com.faiz.phonebook.ui.base;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.faiz.phonebook.R;
import com.faiz.phonebook.data.ServiceProvider;
import com.google.android.material.appbar.CollapsingToolbarLayout;

public abstract class BaseCollapsingToolbarFragment extends BaseToolbarFragment implements BaseCollapsingToolbarMvpView {

    private NestedScrollView nestedScrollView;
    private CollapsingToolbarLayout collapsingToolbar;
    private ImageView imageView;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        nestedScrollView = view.findViewById(R.id.collapsing_nested_scroll_view);
        collapsingToolbar = view.findViewById(R.id.collapsing_toolbar);
        imageView = view.findViewById(R.id.background_image);
    }

    @Override
    protected void setTitle(String title) {
        collapsingToolbar.setTitle(title);
    }

    @Override
    protected void setTitle(int resIdTitle) {
        collapsingToolbar.setTitle(ServiceProvider.getInstance().getResourceHelper().getString(resIdTitle));
    }

    public void loadBackgroundImage(Drawable drawable) {
        if (imageView != null) {
            Glide.with(this)
                    .load(drawable)
                    .apply(RequestOptions.centerInsideTransform())
                    .into(imageView);
        }
    }

    public void loadBackgroundImage(int drawableResId) {
        if (imageView != null) {
            Glide.with(this)
                    .load(drawableResId)
                    .apply(RequestOptions.centerCropTransform())
                    .into(imageView);
        }
    }

    public void loadBackgroundImage(String imageUri) {
        if (imageView != null) {
            Glide.with(this)
                    .load(imageUri)
                    .apply(RequestOptions.centerInsideTransform())
                    .into(imageView);
        }
    }
}
