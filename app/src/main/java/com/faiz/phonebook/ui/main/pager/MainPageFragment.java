package com.faiz.phonebook.ui.main.pager;

import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import com.faiz.phonebook.R;
import com.faiz.phonebook.ui.base.BaseFragment;
import com.faiz.phonebook.ui.base.BasePresenter;
import com.faiz.phonebook.ui.main.MainFragment;
import com.faiz.phonebook.util.ItemClickSupport;
import com.faiz.phonebook.util.ListLoadingDecoration;
import com.faiz.phonebook.widget.EndlessRecyclerView;

import java.util.ArrayList;

import static com.faiz.phonebook.ui.main.MainMvpView.TAB_POSITION_CONTACTS;

public class MainPageFragment extends BaseFragment implements MainPageMvpView {

    private MainPagePresenter presenter = new MainPagePresenter();

    private EndlessRecyclerView recyclerView;
    private MainPageListAdapter adapter;

    public MainPageFragment newInstance(int position) {
        MainPageFragment fragment = new MainPageFragment();
        Bundle arg = new Bundle();
        arg.putInt(ARG_TAB_POSITION, position);
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_main_page_list;
    }

    @Override
    public BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int type = TAB_POSITION_CONTACTS;
        Bundle arg = getArguments();
        if (arg != null && arg.containsKey(ARG_TAB_POSITION)) {
            type = arg.getInt(ARG_TAB_POSITION);
        }
        presenter.setType(type);

        recyclerView = view.findViewById(R.id.main_pager_recycler);
        adapter = new MainPageListAdapter();
        adapter.registerType(TYPE_CREATE_CONTACT, new CreateContactMainDispatcher());
        adapter.registerType(TYPE_CONTACT, new ContactMainDispatcher(adapter));
        adapter.registerType(TYPE_CALL, new CallMainDispatcher(call -> presenter.onRecallButtonPressed(call)));
        adapter.registerType(TYPE_HEADER, new HeaderMainDispatcher());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                presenter.onItemClicked(position);
            }
        });

        recyclerView.setEndlessListener(new EndlessRecyclerView.EndlessRecyclerViewListener() {
            @Override
            public void loadDataFromEnd() {
                presenter.onListEndReached();
            }
        });
        recyclerView.addItemDecoration(new ListLoadingDecoration(getContext(), recyclerView, Color.WHITE));

        presenter.attachView(this, savedInstanceState);
    }

    public void onPageSelected() {
        presenter.onPageSelected();
    }

    @Override
    public void setItems(ArrayList<Object> items) {
        if (items == null) {
            items = new ArrayList<>();
        }
        if (adapter != null) {
            adapter.setItems(items);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void addItems(int position, int count) {
        if (adapter != null && adapter.getItemCount() >= position+count) {
            adapter.notifyItemRangeInserted(position, count);
        }
    }

    @Override
    public void showNoPermissionAlertDialog() {

    }

    @Override
    public void setListLoading(boolean isLoading) {
        if (recyclerView != null) {
            recyclerView.setLoading(isLoading);
        }
    }

    @Override
    public void setEndlessModeEnabled(boolean isEnabled) {
        if (recyclerView != null) {
            recyclerView.setEndless(isEnabled);
        }
    }

    public boolean isFragmentVisible (int position) {
        Fragment parentFragment = getParentFragment();
        if (parentFragment instanceof MainFragment) {
            return ((MainFragment) parentFragment).isTabFragmentVisible(position);
        }
        return false;
    }
}
