package com.faiz.phonebook.widget;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class EndlessRecyclerView extends RecyclerView {
    private boolean isLoading;
    private boolean isEndless = true;
    private EndlessRecyclerViewListener endlessListener;
    private boolean interruptCurrentEvent;
    private int thresholdOffset;
    private boolean disallowIntercept;

    public interface EndlessRecyclerViewListener {
        void loadDataFromEnd();
    }

    public EndlessRecyclerView(Context context) {
        super(context);
    }

    public EndlessRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public EndlessRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onScrolled(int dx, int dy) {
        super.onScrolled(dx, dy);

        if (endlessListener == null || isLoading || !isEndless) {
            return;
        }
        LinearLayoutManager layoutManager = (LinearLayoutManager) getLayoutManager();
        if (layoutManager == null) {
            return;
        }
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
        int lastVisibleItem = firstVisibleItemPosition + getChildCount();
        int threshold = layoutManager.getItemCount() - thresholdOffset;
        if (lastVisibleItem >= threshold) {
            endlessListener.loadDataFromEnd();
        }

    }

    public void setLoading(boolean isLoading){
        this.isLoading = isLoading;
        try {
            invalidateItemDecorations();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void setEndless(boolean isEndless) {
        this.isEndless = isEndless;
    }

    public boolean isEndless() {
        return isEndless;
    }

    public void setEndlessListener(EndlessRecyclerViewListener endlessListener) {
        this.endlessListener = endlessListener;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void interruptTouchEvent(){
        this.interruptCurrentEvent = true;
    }

    public int getThresholdOffset() {
        return thresholdOffset;
    }

    public void setThresholdOffset(int thresholdOffset) {
        this.thresholdOffset = thresholdOffset;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent e) {
        boolean result = super.onInterceptTouchEvent(e);
        if (e.getAction() == MotionEvent.ACTION_DOWN) {
            disallowIntercept = false;
            return false;
        }
        if (disallowIntercept) {
            result = false;
        }
        return result;
    }

    @Override
    public void requestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        this.disallowIntercept = disallowIntercept;
        super.requestDisallowInterceptTouchEvent(disallowIntercept);
    }
}
