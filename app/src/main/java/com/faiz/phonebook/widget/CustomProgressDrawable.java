package com.faiz.phonebook.widget;

import android.graphics.Bitmap;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.BitmapDrawable;
import android.view.animation.AnimationUtils;

public class CustomProgressDrawable extends BitmapDrawable implements Animatable, Runnable {

    public static final int LOOP_MILLS = 2000;
    public static final float FULL_CIRCLE_DEGREES = 360f;
    public static final int Z_CAMERA_COORDINATE = -100;

    private long mStartTicks = 0;
    private boolean mIsRunning = false;
    private Camera mCamera = new Camera();
    private Matrix mMatrix = new Matrix();

    public CustomProgressDrawable(Bitmap bitmap) {
        super(bitmap);
    }

    @Override
    public void draw(Canvas canvas) {
        long l = System.currentTimeMillis();
        if (!mIsRunning) {
            start();
        }
        final Rect bounds = getBounds();
        final float loopPercent = calculateCurrentLoopPercent();
        final float centerX = bounds.centerX();
        final float centerY = bounds.centerY();

        canvas.save();

        mCamera.save();
        mCamera.rotateZ(-FULL_CIRCLE_DEGREES * loopPercent);
        mCamera.setLocation(0, 0, Z_CAMERA_COORDINATE);
        mCamera.getMatrix(mMatrix);

        mMatrix.preTranslate(-centerX, -centerY);
        mMatrix.postTranslate(centerX, centerY);

        mCamera.restore();

        canvas.concat(mMatrix);

        super.draw(canvas);

        canvas.restore();
//        Log.d("Test1", "draw time =" + (System.currentTimeMillis() - l));
    }

    @Override
    public void run() {
    }

    @Override
    public boolean isRunning() {
        return mIsRunning;
    }

    @Override
    public void start() {
        if (!isRunning()) {
            mIsRunning = true;
            mStartTicks = AnimationUtils.currentAnimationTimeMillis();
        }
    }

    @Override
    public void stop() {
        if (isRunning()) {
            mIsRunning = false;
        }
    }

    private float calculateCurrentLoopPercent() {
        float loopPercent = 0f;
        if (isRunning()) {
            float loopMillis = LOOP_MILLS;
            loopPercent = (AnimationUtils.currentAnimationTimeMillis() - mStartTicks) / loopMillis;
            while (loopPercent > 1) {
                loopPercent -= 1;
                mStartTicks += loopMillis;
            }
        }
        return loopPercent;
    }
}
